<?php

    use yii\helpers\Html;
   
    echo '<div class="row">';
    foreach($datos as $registro){
        echo '<div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                    ' . Html::img("@web/assets/img/$registro->foto")
                    . '</div><h3>' . $registro->titulo .'</h3>
                    <p>' . $registro->texto . '</p>
                    <div class="caption">
                        <p>' . Html::a("Leer más", ['site/tcompleto', 'id' => $registro->id], ["class" => "btn btn-primary"]) .
                    '</div>
            </div>';
    }
    echo '</div>';
?>