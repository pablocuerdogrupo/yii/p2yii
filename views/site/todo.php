<?php

    use yii\grid\GridView;

    echo GridView::widget([
        'dataProvider' => $dataProviderN,
        'columns' => [
            'id',
            'titulo',
            'texto'
        ],
        'summary' => 'Listado de noticias:'
    ]);
            
    echo GridView::widget([
        'dataProvider' => $dataProviderA,
        'columns' => [
            'id',
            'titulo',
            'texto'
        ],
        'summary' => 'Listado de artículos:'
    ]); 
?>