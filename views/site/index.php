<?php

    use yii\helpers\Html;

?>
<div class="row">
    <div class="col-lg-4">
        <div class="thumbnail"><?= Html::img("@web/assets/img/" . $datos[0]['foto']); ?></div>
        <h3><?= $datos[0]['titulo']; ?></h3>
        <p><?= $datos[0]['texto']; ?></p>
    </div>
    <div class="col-lg-4">
        <div class="thumbnail"><?= Html::img("@web/assets/img/" . $datos[1]['foto']); ?></div>
        <h3><?= $datos[1]['titulo']; ?></h3>
        <p><?= $datos[1]['texto']; ?></p>
    </div>
</div>